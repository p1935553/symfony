<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
    * @return Post[] Returns an array of Post objects
    */
    public function findAllPublished()
    {
        $db = $this->getEntityManager()->getConnection();
        $sql = '
            SELECT * FROM post p
            WHERE p.published_at < :today
            ORDER BY p.published_at DESC
            ';
        $stmt = $db->prepare($sql);
        $stmt->execute(['today' => date_format(new \DateTime(), "y-m-d H:i:s")]);
        return $stmt->fetchAllAssociative();
    }

    /**
    * @return Post[] Returns an array of Post objects
    */
    public function findLatest()
    {
        $db = $this->getEntityManager()->getConnection();
        $sql = '
            SELECT * FROM post p
            WHERE p.published_at < :today
            ORDER BY p.published_at DESC
            LIMIT 0, 5
            ';
        $stmt = $db->prepare($sql);
        $stmt->execute(['today' => date_format(new \DateTime(), "y-m-d H:i:s")]);
        return $stmt->fetchAllAssociative();
    }

    /**
    * @return Post[] Returns a single Post object
    */
    public function findTitleById($id)
    {
        $db = $this->getEntityManager()->getConnection();
        $sql = '
            SELECT title FROM post p
            WHERE p.id = :id
            ';
        $stmt = $db->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt->fetchOne();
    }

    /**
    * @return Comments[]
    */
    public function findComments($id)
    {
        $db = $this->getEntityManager()->getConnection();
        $sql = '
            SELECT * FROM comment c
            WHERE c.post_id = :id
            ';
        $stmt = $db->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt->fetchOne();
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
