<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    /**
    * @return Comment[] Returns an array of Comment objects
    */
    public function findLatestValid()
    {
        $db = $this->getEntityManager()->getConnection();
        $sql = '
            SELECT * FROM comment c
            WHERE c.created_at < :today AND c.valid = 1
            ORDER BY c.created_at DESC
            LIMIT 0, 5
            ';
        $stmt = $db->prepare($sql);
        $stmt->execute(['today' => date_format(new \DateTime(), "y-m-d H-i-s")]);
        return $stmt->fetchAllAssociative();
    }

    /**
    * @return Comment[] Returns an array of Comment objects
    */
    public function findValidWithPostId($id)
    {
        $db = $this->getEntityManager()->getConnection();
        $sql = '
            SELECT * FROM comment c
            WHERE c.created_at < :today AND c.valid = true AND c.post_id = :id
            ORDER BY c.created_at DESC
            ';
        $stmt = $db->prepare($sql);
        $stmt->execute(['today' => date_format(new \DateTime(), "y-m-d H-i-s"), 'id' => $id]);
        return $stmt->fetchAllAssociative();
    }

    // /**
    //  * @return Comment[] Returns an array of Comment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Comment
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
