<?php

namespace App\Twig;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('postTitle', [$this, 'getArticleTitle']),
        ];
    }

    public function getArticleTitle($id)
    {
        $postRepository = new PostRepository();
        $title = $postRepository->findById($id);
        return $title;
    }
}