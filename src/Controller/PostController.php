<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Knp\Component\Pager\PaginatorInterface; 

class PostController extends AbstractController
{
    /**
     * @Route("/post", name="post_index", methods={"GET"})
     */
    public function index(Request $request, PostRepository $postRepository, PaginatorInterface $paginator): Response
    {
        $donnees = $postRepository->findAll();
        $posts = $paginator->paginate($donnees, $request->query->getInt('page', 1), 5);

        return $this->render('post/index.html.twig', [
            'posts' => $posts,
        ]);
    }  
    
    /**
     * @Route("/admin/post", name="admin_post_index")
     */
    public function post(Request $request, PostRepository $postRepository, PaginatorInterface $paginator): Response
    {
        $donnees = $postRepository->findAll();
        $posts = $paginator->paginate($donnees, $request->query->getInt('page', 1), 5);

        return $this->render('post/index.html.twig', [
            'posts' => $posts,
        ]);
    }
    
    /**
     * @Route("/post/{slug}", name="post_show", methods={"GET", "POST"})
     */
    public function show(Post $post, Request $request, CommentRepository $commentRepository): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        $slug = $post->getSlug();

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setValid(0);
            $comment->setCreatedAt(new \DateTime);
            $comment->setPost($post);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();
        }
        return $this->render('post/show.html.twig', [
            'form', $form,
            'post' => $post,
            'comments' => $commentRepository->findValidWithPostId($post->getId()),
        ]);
    }

        /**
     * @Route("/admin/post/new", name="admin_post_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $slugger = new AsciiSlugger();
            $post->setSlug($slugger->slug($post->getTitle())."-".$post->getId());
            $post->setCreatedAt(new \DateTime());
            $post->setUpdatedAt(new \DateTime());
            $post->setPublishedAt(new \DateTime());
            $entityManager->persist($post);
            $entityManager->flush();

            $post->setSlug($slugger->slug($post->getTitle())."-".$post->getId());
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('admin_post_index');
        }

        return $this->render('post/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/post/{slug}", name="admin_post_show", methods={"GET"})
     */
    public function showAdmin(Post $post): Response
    {
        return $this->render('post/show.html.twig', [
            'post' => $post,
            'comments' => $post->getComments(),
        ]);
    }

    /**
     * @Route("/admin/post/{slug}/edit", name="admin_post_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Post $post): Response
    {
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slugger = new AsciiSlugger();
            $post->setSlug($slugger->slug($post->getTitle())."-".$post->getId());
            $post->setUpdatedAt(new \DateTime);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_post_index');
        }

        return $this->render('post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/post/{id}", name="admin_post_delete", methods={"POST"})
     */
    public function delete(Request $request, Post $post): Response
    {
        if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($post);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_post_index');
    }
}
